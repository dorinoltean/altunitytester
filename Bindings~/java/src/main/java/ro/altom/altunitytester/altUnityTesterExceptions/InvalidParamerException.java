package ro.altom.altunitytester.altUnityTesterExceptions;

public class InvalidParamerException extends AltUnityException {
    /**
     *
     */
    private static final long serialVersionUID = 3793304868452564746L;

    public InvalidParamerException(String message) {
        super(message);
    }
}
