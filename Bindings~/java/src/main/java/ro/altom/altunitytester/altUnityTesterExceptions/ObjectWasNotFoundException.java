package ro.altom.altunitytester.altUnityTesterExceptions;

public class ObjectWasNotFoundException extends AltUnityException {
    /**
     *
     */
    private static final long serialVersionUID = -697687927528781220L;

    public ObjectWasNotFoundException() {
    }

    public ObjectWasNotFoundException(String message) {
        super(message);
    }
}
