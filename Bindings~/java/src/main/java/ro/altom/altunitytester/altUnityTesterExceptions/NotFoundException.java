package ro.altom.altunitytester.altUnityTesterExceptions;

public class NotFoundException extends AltUnityException {
    /**
     *
     */
    private static final long serialVersionUID = 8776239360353861753L;

    public NotFoundException() {
    }

    public NotFoundException(String message) {
        super(message);
    }
}
