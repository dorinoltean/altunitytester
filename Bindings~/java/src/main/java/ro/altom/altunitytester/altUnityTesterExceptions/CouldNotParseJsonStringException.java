package ro.altom.altunitytester.altUnityTesterExceptions;

public class CouldNotParseJsonStringException extends AltUnityException {
    /**
     *
     */
    private static final long serialVersionUID = -3323025080022983490L;

    public CouldNotParseJsonStringException() {
    }

    public CouldNotParseJsonStringException(String message) {
        super(message);
    }
}
