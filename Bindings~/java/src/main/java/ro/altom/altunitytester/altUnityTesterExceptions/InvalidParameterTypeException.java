package ro.altom.altunitytester.altUnityTesterExceptions;

public class InvalidParameterTypeException extends AltUnityException {
    /**
     *
     */
    private static final long serialVersionUID = -5648566121586724588L;

    public InvalidParameterTypeException() {
    }

    public InvalidParameterTypeException(String message) {
        super(message);
    }
}
